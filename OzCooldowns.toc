## Interface: 60000
## DefaultState: Enabled
## Title: |cFF00FFFFOz|r|cFFFFFFFFCooldowns|r
## Author: Azilroka, Nimaear
## Version: 3.26
## OptionalDeps: Tukui, ElvUI, DSM, AddOnSkins
## SavedVariables: OzCooldownsOptions
## SavedVariablesPerCharacter: OzCooldownsSpellCDs

Libs\Load_Libs.xml
OzCooldowns.lua