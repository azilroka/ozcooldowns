local AddOnName, Engine = ...
local LSM, ES, EP, AS

local Defaults = {
	["Spacing"] = 10,
	["Size"] = 36,
	["Vertical"] = false,
	["Tooltips"] = true,
	["Announce"] = true,
	["MinimumDuration"] = 600,
	["StatusBar"] = true,
	["StatusBarTexture"] = IsAddOnLoaded("ElvUI") and ElvUI[1].private.general.normTex or IsAddOnLoaded("Tukui") and "Tukui" or "Blizzard",
	["StatusBarTextureColor"] = { ["r"] = .24, ["g"] = .54, ["b"] = .78 },
	["DurationFont"] = IsAddOnLoaded("ElvUI") and ElvUI[1].db.general.font or IsAddOnLoaded("Tukui") and "Tukui Pixel" or "Arial Narrow",
	["DurationFontSize"] = 12,
	["DurationFontFlag"] = IsAddOnLoaded("Tukui") and "MONOCHROMEOUTLINE" or "OUTLINE",
	["Mode"] = "HIDE",
	["FadeMode"] = "None",
	["SortByDuration"] = true,
	["DurationText"] = true,
}

OzCooldownsOptions = CopyTable(Defaults)

local ClassCooldowns = {
	["DEATHKNIGHT"] = {
		42650,	-- Army of the Dead
		43265,	-- Death and Decay
		45529,	-- Blood Tap
		46584,	-- Raise Dead
		47476,	-- Strangulate
		47528,	-- Mind Freeze
		47568,	-- Empower Rune Weapon
		48707,	-- Anti-magic shield
		48743,	-- Death Pact
		48792,	-- Icebound Fortitude
		48982,	-- Rune Tap
		49016,	-- Hysteria
		49028,	-- Dancing Runic Blade
		49203,	-- Hungering Cold
		49206,	-- Summon Gargoyle
		49222,	-- Bone Shield
		49576,	-- Death Grip
		49796,	-- Deathchill
		51052,	-- Anti-magic zone
		51271,	-- Unbreakable Armor
		55233,	-- Vampiric Blood
		56222,	-- Dark Command
		57330,	-- Horn of Winter
		77575,	-- Outbreak
		77606,	-- Dark Simulacrum
		108911,	-- Gorefiends Grip
	},
	["DRUID"] = {
		740,	-- Tranquility
		1850,	-- Dash
		5209,	-- Challenging Roar
		5217,	-- Tiger's Fury
		16689,	-- Nature's Grasp
		16979,	-- Feral Charge - Bear
		17116,	-- Nature's Swiftness
		18562,	-- Swiftmend
		20484,	-- Rebirth
		22842,	-- Frenzied Regeneration
		33831,	-- Force of Nature
		48438,	-- Wild Growth
		48505,	-- Starfall
		49376,	-- Feral Charge - Cat
		50516,	-- Typhoon
		50334,	-- Berserk
		61336,	-- Survival Instincts
		77764,	-- Stampeding Roar
	},
	["HUNTER"] = {
		781,	-- Disengage
		1499,	-- Freezing Trap
		1543,	-- Flare
		3045,	-- Rapid Fire
		3674,	-- Black Arrow
		5384,	-- Feign Death
		13795,	-- Immolation Trap
		13809,	-- Ice Trap
		13813,	-- Explosive Trap
		19263,	-- Deterrence
		19386,	-- Wyvern Sting
		19503,	-- Scatter Shot
		19574,	-- Bestial Wrath
		19577,	-- Intimidation
		23989,	-- Readiness
		34026,	-- Kill Command
		34477,	-- Misdirection
		34490,	-- Silencing Shot
		34600,	-- Snake Trap
		51753,	-- Camouflage
		53209,	-- Chimera Shot
		53271,	-- Master's Call
		53301,	-- Explosive Shot
		60192,	-- Freezing Trap Launcher
		77769,	-- Trap Launcher
		82726,	-- Fervor
		82939,	-- Explosive Trap Launcher
		82941,	-- Ice Trap Launcher
		82945,	-- Immolation Trap Launcher
		82948,	-- Snake Trap Launcher
		109248,	-- Binding Shot
		109259,	-- Powershot
		109304,	-- Exhilaration
		117050,	-- Glaive Toss
		120360,	-- Barrage
		120679,	-- Dire Beast
		120697,	-- Lynx Rush
		121818,	-- Stampede
		130392,	-- Blink Strike
		131894,	-- A Murder of Crows
	},
	["MAGE"] = {
		66,		-- Invisibility
		120,	-- Cone of Cold
		122,	-- Frost Nova
		543,	-- Magic Protection (Fire/Frost Ward)
		1463,	-- Mana Shield
		1953,	-- Blink
		2139,	-- Counterspell
		11113,	-- Blast Wave
		11129,	-- Combustion
		11426,	-- Ice Barrier
		11958,	-- Cold Snap
		12042,	-- Arcane Power
		12043,	-- Presence of Mind
		12051,	-- Evocation
		12472,	-- Icy Veins
		31661,	-- Dragon Breath
		31687,	-- Water Elemental
		44572,	-- Deep Freeze
		45438,	-- Ice Block
		55342,	-- Mirror Images
		80353,	-- Time Warp
		82676,	-- Ring of Frost
		82731,	-- Flame Orb
	},
	["MONK"] = {
		101545,	-- Flying Serpent Kick
		101643,	-- Transcendence
		107428,	-- Rising Sun Kick
		112783,	-- Diffuse Magic
		113656,	-- Fists of Fury
		115072,	-- Expel Harm
		115078,	-- Paralysis
		115080,	-- Touch of Death
		115098,	-- Chi Wave
		115151,	-- Renewing Mist
		115176,	-- Zen Meditation
		115203,	-- Fortifying Brew
		115213,	-- Avert Harm
		115288,	-- Energizing Brew
		115295,	-- Guard
		115308,	-- Elusive Brew
		115310,	-- Revival
		115313,	-- Summon Jade Serpent Statue
		115315,	-- Summon Black Ox Statue
		115450,	-- Detox
		115546,	-- Provoke
		116680,	-- Thunder Focus Tea
		116705,	-- Spear Hand Strike
		116740,	-- Tigereye Brew
		116847,	-- Rushing Jade Wind
		116849,	-- Life Cocoon
		117368,	-- Grapple Weapon
		119381,	-- Leg Sweep
		119392,	-- Charging Ox Wave
		119996,	-- Transcendence: Transfer
		121253,	-- Keg Smash
		122057,	-- Clash
		122278,	-- Dampen Harm
		122470,	-- Touch of Karma
		123761,	-- Mana Tea (Glyphed)
		123904,	-- Invoke Xuen, the White Tiger
	},
	["PALADIN"] = {
		498,	-- Divine Protection
		633,	-- Lay on Hands
		642,	-- Divine Shield
		853,	-- Hammer of Justice
		1022,	-- Hand of Protection
		1038,	-- Hand of Salvation
		1044,	-- Hand of Freedom
		2812,	-- Holy Wrath
		6940,	-- Hand of Sacrifice
		20066,	-- Repentance
		20216,	-- Divine Favor
		20473,	-- Holy Shock
		20925,	-- Sacred Shield
		26573,	-- Consecration
		31884,	-- Avenging Wrath
		31821,	-- Aura Mastery
		31850,	-- Ardent Defender
		54428,	-- Divine Plea
		82327,	-- Holy Radiance
		85222,	-- Light of Dawn
		85285,	-- Rebuke
		85696,	-- Zealotry
		86150,	-- Guardian of Ancient Kings - Prot
		86698,	-- Guardian of Ancient Kings - Holy
		86669,	-- Guardian of Ancient Kings - Holy
	},
	["PRIEST"] = {
		17,		-- Power Word: Shield
		527,	-- Purify
		586,	-- Fade
		724,	-- Lightwell
		6346,	-- Fear Ward
		8092,	-- Mind Blast
		8122,	-- Psychic Scream
		10060,	-- Power Infusion
		14914,	-- Holy Fire
		15286,	-- Vampiric Embrace
		15487,	-- Silence
		19236,	-- Desperate Prayer
		32375,	-- Mass Dispel
		32379,	-- Shdow Word: Death
		33076,	-- Prayer of Mending
		33206,	-- Pain Suppression
		34433,	-- Shadowfiend
		34861,	-- Circle of Healing
		47540,	-- Penance
		47585,	-- Dispersion
		47788,	-- Guardian Spirit
		62618,	-- Power Word: Barrier
		64901,	-- Hymm of Hope
		64044,	-- Psychic Horror
		64843,	-- Divine Hymm
		73325,	-- Leap of Faith
		81206,	-- Chakra: Sanctuary
		81209,	-- Chakra: Chastise
		81208,	-- Chakra: Serenity
		81700,	-- Archangel
		88625,	-- Holy Word: Chastise
		88684,	-- Holy Word: Serenity
		88685,	-- Holy Word: Sanctuary
		89485,	-- Inner Focus
		108920,	-- Void Tendrils
		108921,	-- Psyfiend
		108968,	-- Void Shift
		109964,	-- Spirit Shell
		110744,	-- Divine Star
		120517,	-- Halo
		121135,	-- Cascade
		121536,	-- Angelic Feather
		123040,	-- Mindbender
		129250, -- Power Word: Solace
	},
	["ROGUE"] = {
		408,	-- Kidney Shot
		1725,	-- Distract
		1766,	-- Kick
		1776,	-- Gouge
		1784,	-- Stealth
		1856,	-- Vanish
		2094,	-- Blind
		2983,	-- Sprint
		5277,	-- Evasion
		5938,	-- Shiv
		13750,	-- Adrenaline Rush
		13877,	-- Blade Flurry
		14183,	-- Premeditation
		14185,	-- Preparation
		31224,	-- Cloak of Shadows
		36554,	-- Shadowstep
		51690,	-- Killing Spree
		51713,	-- Shadow Dance
		51722,	-- Dismantle
		57934,	-- Tricks
		73981,	-- Redirect
		74001,	-- Combat Readiness
		76577,	-- Smoke Bomb
		79140,	-- Vendetta
		114842,	-- Shadow Walk
		114018,	-- Shroud of Concealment
		121471,	-- Shadow Blades
		137619,	-- Marked for Death
	},
	["SHAMAN"] = {
		421,	-- Chain Lightning
		2825,	-- Bloodlust
		8042,	-- Earth Shock
		8050,	-- Flame Shock
		8056,	-- Frost Shock
		8177,	-- Grounding Totem
		16166,	-- Elemental Mastery
		16188,	-- Ancestral Swiftness
		16190,	-- Mana Tide Totem
		30823,	-- Shamanistic Rage
		32182,	-- Heroism
		51485,	-- Earthgrab Totem
		51490,	-- Thunderstorm
		51505,	-- Lava Burst
		51514,	-- Hex
		51533,	-- Feral Spirit
		55198,	-- Tidal Force
		57994,	-- Wind Shear
		60103,	-- Lava Lash
		61295,	-- Riptide
		73680,	-- Unleash Elements
		73899,	-- Primal Strike
		73920,	-- Healing Rain
		77130,	-- Purify Spirit
		79206,	-- Spiritwalker's Grace
		98008,	-- Spirit Link Totem
		108270,	-- Stone Bulwark Totem
		108271,	-- Astral Shift
		108273,	-- Windwalk Totem
		108280,	-- Healing Tide Totem
		108281,	-- Ancestral Guidance
		108285,	-- Call of Elements
		117014,	-- Elemental Blast
		165339,	-- Ascendence
	},
	["WARLOCK"] = {
		5484,	-- Howl of Terror
		6789,	-- Mortal Coil
		18540,	-- Summon Doomguard / Infernal
		18708,	-- Fel Domination
		29858,	-- Soulshatter
		48020,	-- Demonic Circle: Teleport
		77801,	-- Dark Soul
		104773,	-- Unending Resolve
		108505,	-- Archimonde's Vengeance
		108416,	-- Sacrificial Pact
		110913,	-- Dark Bargain
		113858,	-- Dark Soul: Instability
		113860,	-- Dark Soul: Misery
		113861,	-- Dark Soul: Knowledge
		114189,	-- Health Funnel (Glyphed)
	},
	["WARRIOR"] = {
		100,	-- Charge
		355,	-- Taunt
		676,	-- Disarm
		871,	-- Shield Wall
		1160,	-- Demoralizing Roar
		1719,	-- Recklessness
		2565,	-- Shield Block
		3411,	-- Intervene
		5246,	-- Intimidating Shout
		6343,	-- Thunder Clap
		6544,	-- Heroic Leap
		6552,	-- Pummel
		12292,	-- Bloodbath
		12328,	-- Sweeping Strikes
		12975,	-- Last Stand
		18499,	-- Berserker Rage
		23920,	-- Spell Reflection
		23922,	-- Shield Slam
		46924,	-- Bladestorm
		46968,	-- Shockwave
		55694,	-- Enraged Regeneration
		57755,	-- Heroic Throw
		64382,	-- Shattering Throw
		86346,	-- Colossus Smash
		102060,	-- Disrupting Shout
		103840,	-- Impending Victory
		107556,	-- Staggering Shout
		107570,	-- Storm Bolt
		107574,	-- Avatar
		114028,	-- Mass Spell Reflection
		114029,	-- Safeguard
		114030,	-- Vigilance
		114192,	-- Mocking Banner
		114203,	-- Demoralizing Roar
		114207,	-- Skull Banner
		118000,	-- Dragon Roar
		118038,	-- Die by the Sword
	},
	["RACE"] = {
		["BloodElf"] = {
			25046,
			50613,
			28730,
		},
		["Draenei"] = {
			59545,
			59543,
			59548,
			59542,
			59544,
			59547,
			28880,
		},
		["Dwarf"] = {
			20594,
		},
		["Gnome"] = {
			20589,
		},
		["Goblin"] = {
			69070,
		},
		["Human"] = {
			59752,
		},
		["NightElf"] = {
			58984,
		},
		["Orc"] = {
			33697,
			33702,
			20572,
		},
		["Pandaren"] = {
		},
		["Scourge"] = {
			7744,	-- Will of the Forsaken
			20577,	-- Cannibalize
		},
		["Tauren"] = {
			20549,
		},
		["Troll"] = {
			26297,
		},
		["Worgen"] = {
			68992,	-- Darkflight
			68996,	-- Two Forms
		},
	},
	["PET"] = {
		["HUNTER"] = {
			53401,	-- Rabid
			90355,	-- Ancient Hysteria
			126393,	-- Eternal Guardian
			159956,	-- Dust of Life
		},
		["WARLOCK"] = {
			6360,	-- Succubus Whiplash
			19647,	-- Felhunter Spell Lock
			30151,	-- Felguard Pursuit
			89766,	-- Felguard Axe Toss
			89751,	-- Felguard Felstorm
			115770,	-- Shivarra Fellash
			115781,	-- Observer Optical Blast
			115831,	-- Wrathguard Wrathstorm
			171018, -- Meteor Strike (Infernal / Abyssal)
			171138, -- Shadow Lock (Doomguard / Terrorguard)
		},
	},
}

local OzCDHider = CreateFrame('Frame', "OzCDHider", UIParent)

local OzCooldowns = CreateFrame("Frame", "OzCooldowns", UIParent)
OzCooldowns.Title = GetAddOnMetadata(AddOnName, "Title")
OzCooldowns.Version = GetAddOnMetadata(AddOnName, "Version")

local pairs, format, ceil, tinsert = pairs, format, ceil, tinsert
local GetTime, GetSpellInfo, GetSpellTexture, GetSpellCooldown = GetTime, GetSpellInfo, GetSpellTexture, GetSpellCooldown

local MyRace = select(2, UnitRace("player"))
local MyClass = select(2, UnitClass("player"))

local PlayerCooldowns = {}
local Cooldowns = {}
local CooldownFrames = {}

local function isPetSpell(SpellID)
	if IsSpellKnown(SpellID) then
		return false
	elseif IsSpellKnown(SpellID, true) then
		return true
	else
		return false
	end
end

local i, k, v, SpellID

for k, SpellID in pairs(ClassCooldowns[MyClass]) do
	if GetSpellInfo(SpellID) then
		tinsert(PlayerCooldowns, SpellID)
	end
end

for k, SpellID in pairs(ClassCooldowns["RACE"][MyRace]) do
	if IsSpellKnown(SpellID) then
		tinsert(PlayerCooldowns, SpellID)
	end
end

if MyClass == "WARLOCK" or MyClass == "HUNTER" then
	for k, SpellID in pairs(ClassCooldowns["PET"][MyClass]) do
		if GetSpellInfo(SpellID) then
			tinsert(PlayerCooldowns, SpellID)
		end
	end
end

OzCooldownsSpellCDs = {}

for k, v in pairs(PlayerCooldowns) do
	OzCooldownsSpellCDs[GetSpellInfo(v)] = true
end

local SpellDefaults = CopyTable(OzCooldownsSpellCDs)

sort(PlayerCooldowns, function (a, b)
	return GetSpellInfo(a) < GetSpellInfo(b)
end)

function OzCooldowns:FindCooldown(SpellID)
	for _, Frame in pairs(Cooldowns) do
		if SpellID == Frame.SpellID then
			if OzCooldownsOptions["Mode"] == "HIDE" then
				OzCooldowns:DelayedEnableCooldown(Frame)
			else
				OzCooldowns:EnableCooldown(Frame)
			end
			break
		end
	end
end

function OzCooldowns:DelayedEnableCooldown(self)
	self:SetParent(OzCDHider)
	self:Show()
	self:SetAlpha(0)
	self:EnableMouse(false)
	self:SetScript("OnUpdate", function(self, elapsed)
		local Start, Duration = GetSpellCooldown(self.SpellID)
		local CurrentDuration = (Start + Duration - GetTime())
		if floor(CurrentDuration) <= OzCooldownsOptions["MinimumDuration"] then
			OzCooldowns:EnableCooldown(self)
			OzCooldowns:Position()
		end
	end)
end

function OzCooldowns:EnableCooldown(self)
	self.Enabled = true
	self:SetParent(OzCooldowns)
	self:Show()
	self:SetAlpha(1)
	self:EnableMouse(OzCooldownsOptions["Tooltips"] or OzCooldownsOptions["Announce"])
	self.Icon:SetDesaturated(false)
	self:SetScript("OnUpdate", function(self, elapsed)
		local Start, Duration = GetSpellCooldown(self.SpellID)
		if Duration and Duration > 1 then
			local CurrentDuration = (Start + Duration - GetTime())
			local Normalized = CurrentDuration / Duration
			self.StatusBar:SetValue(Normalized)
			self.DurationText:SetTextColor(1, 1, 0)
			if CurrentDuration > 60 then
				self.DurationText:SetFormattedText("%dm", ceil(CurrentDuration / 60))
			elseif CurrentDuration <= 60 and CurrentDuration > 10 then
				self.DurationText:SetFormattedText("%d", CurrentDuration)
			elseif CurrentDuration <= 10 and CurrentDuration > 0 then
				self.DurationText:SetTextColor(1, 0, 0)
				self.DurationText:SetFormattedText("%.1f", CurrentDuration)
			end
			if OzCooldownsOptions["FadeMode"] == "GreenToRed" then
				self.StatusBar:SetStatusBarColor(1 - Normalized, Normalized, 0)
			elseif OzCooldownsOptions["FadeMode"] == "RedToGreen" then
				self.StatusBar:SetStatusBarColor(Normalized, 1 - Normalized, 0)
			end
			if OzCooldownsOptions["StatusBar"] then
				self.StatusBar:Show()
				if OzCooldownsOptions["DurationText"] then
					self.DurationText:Show()
				else
					self.DurationText:Hide()
				end
				self.Cooldown:Hide()
			else
				self.StatusBar:Hide()
				self.DurationText:Hide()
				self.Cooldown:Show()
				self.Cooldown:SetCooldown(Start, Duration)
			end
			if CurrentDuration < 1 then
				self.Cooldown:Show()
				self.Cooldown:SetCooldown(0, 0)
			end
		else
			OzCooldowns:DisableCooldown(self)
			OzCooldowns:Position()
		end
	end)
end

function OzCooldowns:DisableCooldown(self)
	self:SetParent(OzCooldowns)
	self.Enabled = false
	self.StatusBar:Hide()
	self.Cooldown:Hide()
	self.DurationText:Hide()
	if OzCooldownsOptions["Mode"] == "HIDE" then
		self:Hide()
	else
		self:Show()
		self:SetAlpha(.3)
	end
	self:EnableMouse(false)
	self.Icon:SetDesaturated(true)
	self:SetScript("OnUpdate", nil)
end

function OzCooldowns:Position()
	local Vertical, Spacing, Size = OzCooldownsOptions["Vertical"], OzCooldownsOptions["Spacing"], OzCooldownsOptions["Size"]
	local Font, StatusBarTexture, Color = LSM:Fetch("font", OzCooldownsOptions["DurationFont"]), LSM:Fetch('statusbar', OzCooldownsOptions["StatusBarTexture"]), OzCooldownsOptions["StatusBarTextureColor"]
	local xSpacing = Vertical and 0 or Spacing
	local ySpacing = Vertical and -(Spacing + (OzCooldownsOptions["StatusBar"] and 8 or 0)) or 0
	local AnchorPoint = Vertical and "BOTTOMLEFT" or "TOPRIGHT"
	local LastFrame = OzCooldowns
	local Index = 0

	if OzCooldownsOptions["SortByDuration"] and OzCooldownsOptions["Mode"] == "HIDE" then
		sort(Cooldowns, function (a, b)
			local aStart, aDuration = GetSpellCooldown(a.SpellID)
			local bStart, bDuration = GetSpellCooldown(b.SpellID)
			return (aStart + aDuration) < (bStart + bDuration)
		end)
	end

	for i = 1, #Cooldowns do
		local Frame = Cooldowns[i]

		if Frame.Enabled then
			local Start, Duration = GetSpellCooldown(Frame.SpellID)
			local CurrentDuration = (Start + Duration - GetTime())
			Frame:SetSize(Size, Size)
			if AS then
				Frame.StatusBar:SetSize(Size - (AS.PixelPerfect and 2 or 4), 4)
			else
				Frame.StatusBar:SetSize(Size - 2, 4)
			end
			Frame.DurationText:SetFont(Font, OzCooldownsOptions["DurationFontSize"], OzCooldownsOptions["DurationFontFlag"])
			Frame.StatusBar:SetStatusBarTexture(StatusBarTexture)
			Frame.StatusBar:SetStatusBarColor(Color["r"], Color["g"], Color["b"])
			if Frame.SpellID == 88682 or Frame.SpellID == 88684 or Frame.SpellID == 88685 then
				Frame.Icon:SetTexture(select(3, GetSpellInfo(88625)))
			else
				Frame.Icon:SetTexture(select(3, GetSpellInfo(Frame.SpellID)))
			end
			if OzCooldownsOptions["Mode"] == "HIDE" then
				Frame:ClearAllPoints()
				Frame:SetPoint("TOPLEFT", LastFrame, Index == 0 and "TOPLEFT" or AnchorPoint, xSpacing, ySpacing)
				LastFrame = Frame
			end
			Index = Index + 1
		else
			if OzCooldownsOptions["Mode"] == "DIM" and IsSpellKnown(Frame.SpellID, isPetSpell(Frame.SpellID)) and not self.DimPositioned then
				Frame:ClearAllPoints()
				Frame:SetPoint("TOPLEFT", LastFrame, Index == 0 and "TOPLEFT" or AnchorPoint, xSpacing, ySpacing)
				LastFrame = Frame
				Index = Index + 1
			end
			if OzCooldownsOptions["Mode"] == "DIM" and IsSpellKnown(Frame.SpellID, isPetSpell(Frame.SpellID)) and self.DimPositioned then
				Index = Index + 1
			end
			if OzCooldownsOptions["Mode"] == "HIDE" then
				Frame:SetParent(OzCDHider)
			end
		end
	end

	if OzCooldownsOptions["Vertical"] then
		OzCooldowns:SetHeight(Size * Index + (Index + 1) * ySpacing)
	else
		OzCooldowns:SetWidth(Size * Index + (Index + 1) * xSpacing)
	end
end

function OzCooldowns:CreateCooldownFrame(SpellID)
	local Frame = CreateFrame("Button", nil, OzCooldowns)
	Frame:RegisterForClicks('AnyUp')
	Frame:SetSize(OzCooldownsOptions["Size"], OzCooldownsOptions["Size"])

	Frame.Icon = Frame:CreateTexture(nil, "ARTWORK")
	Frame.Icon:SetPoint('TOPLEFT', 2, -2)
	Frame.Icon:SetPoint('BOTTOMRIGHT', -2, 2)
	Frame.Icon:SetTexCoord(.08, .92, .08, .92)
	Frame.Icon:SetTexture(select(3, GetSpellInfo(SpellID)))

	Frame.DurationText = Frame:CreateFontString(nil, "OVERLAY")
	Frame.DurationText:SetFont(LSM:Fetch("font", OzCooldownsOptions["DurationFont"]), OzCooldownsOptions["DurationFontSize"], OzCooldownsOptions["DurationFontFlag"])
	Frame.DurationText:SetTextColor(1, 1, 0)
	Frame.DurationText:SetPoint("CENTER", Frame, "CENTER", 0, 0)

	Frame.StatusBar = CreateFrame("StatusBar", nil, Frame)
	Frame.StatusBar:SetSize(OzCooldownsOptions["Size"] - 2, 4)
	Frame.StatusBar:SetStatusBarTexture(LSM:Fetch('statusbar', OzCooldownsOptions["StatusBarTexture"]))
	Frame.StatusBar:SetStatusBarColor(OzCooldownsOptions["StatusBarTextureColor"]["r"], OzCooldownsOptions["StatusBarTextureColor"]["g"], OzCooldownsOptions["StatusBarTextureColor"]["b"])
	Frame.StatusBar:SetPoint("TOP", Frame, "BOTTOM", 0, -1)
	Frame.StatusBar:SetMinMaxValues(0, 1)

	Frame.Cooldown = CreateFrame("Cooldown", nil, Frame, "CooldownFrameTemplate")
	Frame.Cooldown:SetAllPoints(Frame)

	Frame.SpellID = SpellID

	if AS then
		AS:SetTemplate(Frame)
		AS:CreateShadow(Frame)
		Frame.Icon:SetInside()
		AS:CreateBackdrop(Frame.StatusBar)
		AS:CreateShadow(Frame.StatusBar.Backdrop)
		Frame.StatusBar:SetPoint("TOP", Frame, "BOTTOM", 0, (AS.PixelPerfect and -1 or -3))
	else
		local BG =	{
			bgFile = [[Interface\Buttons\WHITE8x8]],
			edgeFile = [[Interface\Buttons\WHITE8x8]],
			tile = false, tileSize = 0, edgeSize = 1,
			insets = { left = 1, right = 1, top = 1, bottom = 1},
		}

		Frame:SetBackdrop(BG)
		Frame:SetBackdropColor(0, 0, 0, 1)
		Frame:SetBackdropBorderColor(.6, .6, .6)

		Frame.StatusBar.BG = CreateFrame('Frame', nil, Frame.StatusBar)
		Frame.StatusBar.BG:SetFrameLevel(Frame.StatusBar:GetFrameLevel() - 1)
		Frame.StatusBar.BG:SetBackdrop(BG)
		Frame.StatusBar.BG:SetBackdropColor(0, 0, 0, 1)
		Frame.StatusBar.BG:SetBackdropBorderColor(.6, .6, .6)
		Frame.StatusBar.BG:SetPoint('TOPLEFT', -1, 1)
		Frame.StatusBar.BG:SetPoint('BOTTOMRIGHT', 1, -1)
	end

	if not (ElvUI or Tukui) then
		Frame:RegisterForDrag('LeftButton')
		Frame:SetScript('OnDragStart', function(self) self:GetParent():StartMoving() end)
		Frame:SetScript('OnDragStop', function(self) self:GetParent():StopMovingOrSizing() end)
	end

	if ES then
		ES:RegisterShadow(Frame.backdrop.shadow)
		ES:RegisterShadow(Frame.StatusBar.backdrop.shadow)
	end

	Frame:SetScript('OnEnter', function(self, ...)
		if OzCooldownsOptions["Tooltips"] then
			GameTooltip:SetOwner(self, 'ANCHOR_CURSOR')
			GameTooltip:ClearLines()
			GameTooltip:SetSpellByID(self.SpellID)
			GameTooltip:Show()
		end
	end)
	Frame:SetScript('OnLeave', GameTooltip_Hide)
	Frame:SetScript('OnClick', function(self)
		if OzCooldownsOptions["Announce"] then
			local Channel = IsInRaid() and "RAID" or IsPartyLFG() and "INSTANCE_CHAT" or IsInGroup() and "PARTY" or "SAY"
			local Start, Duration = GetSpellCooldown(self.SpellID)
			local CurrentDuration = (Start + Duration - GetTime())
			local TimeRemaining
			if CurrentDuration > 60 then
				TimeRemaining = format("%d m", ceil(CurrentDuration / 60))
			elseif CurrentDuration <= 60 and CurrentDuration > 10 then
				TimeRemaining = format("%d s", CurrentDuration)
			elseif CurrentDuration <= 10 and CurrentDuration > 0 then
				TimeRemaining = format("%.1f s", CurrentDuration)
			end
			SendChatMessage(format("My %s will be off cooldown in %s", GetSpellInfo(self.SpellID), TimeRemaining), Channel)
		end
	end)

	OzCooldowns:DisableCooldown(Frame)
	return Frame
end

function OzCooldowns:BuildCooldowns()
	wipe(Cooldowns)

	local i = 1
	for k, v in pairs(PlayerCooldowns) do
		if GetSpellInfo(v) then
			CooldownFrames[v] = CooldownFrames[v] or OzCooldowns:CreateCooldownFrame(v)
			if CooldownFrames[v].Enabled then
				CooldownFrames[v]:EnableMouse(OzCooldownsOptions["Tooltips"] or OzCooldownsOptions["Announce"])
			end

			local SpellName = GetSpellInfo(v)
			if OzCooldownsSpellCDs[SpellName] then
				Cooldowns[i] = CooldownFrames[v]
				i = i + 1
			elseif CooldownFrames[v] then
				OzCooldowns:DisableCooldown(CooldownFrames[v])
				CooldownFrames[v]:Hide()
			end
		end
	end

	sort(Cooldowns, function (a, b)
		local aDuration = GetSpellBaseCooldown(a.SpellID)
		local bDuration = GetSpellBaseCooldown(b.SpellID)
		if aDuration == nil and bDuration == nil then
			return true
		end
		if aDuration == nil then
			return false
		end
		if bDuration == nil then
			return true
		end
		return aDuration < bDuration
	end)

	if OzCooldownsOptions["Mode"] == "DIM" then
		for k, v in pairs(Cooldowns) do
			v:Hide()
			if IsSpellKnown(v.SpellID, isPetSpell(v.SpellID)) then
				self:DisableCooldown(v)
			end
		end
	end

	self.DimPositioned = false
	OzCooldowns:Position()
	self.DimPositioned = true

	if OzCooldownsOptions["Mode"] == "DIM" then
		for k, v in pairs(Cooldowns) do
			if select(2, GetSpellCooldown(v.SpellID)) then
				self:EnableCooldown(v)
			end
		end
	end
end

local function OrderedPairs(t, f)
	local a = {}
	for n in pairs(t) do tinsert(a, n) end
	sort(a, f)
	local i = 0
	local iter = function()
		i = i + 1
		if a[i] == nil then return nil
			else return a[i], t[a[i]]
		end
	end
	return iter
end

local SpellOptions = {}
function OzCooldowns:GenerateSpellOptions()
	wipe(SpellOptions)
	local i = 1
	for k, v in OrderedPairs(OzCooldownsSpellCDs) do
		local SpellName = k
		if SpellName then
			SpellOptions[SpellName] = {
				order = i,
				type = "toggle",
				name = SpellName,
			}
			i = i + 1
		end
	end
	return SpellOptions
end

function OzCooldowns:GetOptions()
	local Options = {
		type = "group",
		name = OzCooldowns["Title"],
		order = 101,
		get = function(info) return OzCooldownsOptions[info[#info]] end,
		set = function(info, value) OzCooldownsOptions[info[#info]] = value; OzCooldowns:BuildCooldowns() end,
		args = {
			spells = {
				order = 1,
				type = "group",
				name = "Spells",
				guiInline = true,
				args = OzCooldowns:GenerateSpellOptions(),
				get = function(info) return OzCooldownsSpellCDs[info[#info]] end,
				set = function(info, value)
					OzCooldownsSpellCDs[info[#info]] = value;
					OzCooldowns:BuildCooldowns()
				end,
			},
			duration = {
				order = 2,
				type = "group",
				name = "Duration",
				guiInline = true,
				args = {
					DurationFont = {
						type = "select", dialogControl = 'LSM30_Font',
						order = 1,
						name = "Font",
						values = AceGUIWidgetLSMlists.font,
						disabled = function() return not OzCooldownsOptions["StatusBar"] end,
					},
					DurationFontSize = {
						order = 2,
						name = "Font Size",
						type = "range",
						min = 8, max = 22, step = 1,
						disabled = function() return not OzCooldownsOptions["StatusBar"] end,
					},
					DurationFontFlag = {
						name = 'Font Outline',
						order = 3,
						type = "select",
						values = {
							["NONE"] = 'None',
							["OUTLINE"] = 'OUTLINE',
							["MONOCHROME"] = 'MONOCHROME',
							["MONOCHROMEOUTLINE"] = 'MONOCROMEOUTLINE',
							["THICKOUTLINE"] = 'THICKOUTLINE',
						},
						disabled = function() return not OzCooldownsOptions["StatusBar"] end,
					},
					DurationText = {
						order = 4,
						type = "toggle",
						name = "Duration Text",
						disabled = function() return OzCooldownsOptions["Mode"] == "DIM" end,
					},
					SortByDuration = {
						order = 5,
						type = "toggle",
						name = "Sort by Current Duration",
						disabled = function() return OzCooldownsOptions["Mode"] == "DIM" end,
					},
					MinimumDuration = {
						order = 6,
						type = "range",
						name = "Minimum Duration Visibility",
						min = 2, max = 600, step = 1,
					},
				},
			},
			icons = {
				order = 3,
				type = "group",
				name = "Icons",
				guiInline = true,
				args = {
					Vertical = {
						order = 1,
						type = "toggle",
						name = "Vertical",
					},
					Tooltips = {
						order = 2,
						type = "toggle",
						name = "Tooltips",
					},
					Announce = {
						order = 3,
						type = "toggle",
						name = "Announce on Click",
					},
					Size = {
						order = 4,
						type = "range",
						width = "full",
						name = "Size",
						min = 30, max = 60, step = 1,
					},
					Spacing = {
						order = 5,
						type = "range",
						width = "full",
						name = "Spacing",
						min = 0, max = 20, step = 1,
					},
					Mode = {
						order = 6,
						type = "select",
						name = "Dim or Hide",
						width = "full",
						values = {
							["DIM"] = "DIM",
							["HIDE"] = "HIDE",
						},
					},
				},
			},
			statusbar = {
				order = 4,
				type = "group",
				name = "Status Bar",
				guiInline = true,
				args = {
					StatusBar = {
						order = 1,
						type = "toggle",
						name = "Enabled",
					},
					StatusBarTexture = {
						type = "select", dialogControl = 'LSM30_Statusbar',
						order = 2,
						name = "Texture",
						values = AceGUIWidgetLSMlists.statusbar,
						disabled = function() return not OzCooldownsOptions["StatusBar"] end,
					},
					FadeMode = {
						order = 3,
						type = "select",
						name = "Fade Mode",
						values = {
							["None"] = "None",
							["RedToGreen"] = "Red to Green",
							["GreenToRed"] = "Green to Red",
						},
					},
					StatusBarTextureColor = {
						type = "color",
						order = 4,
						name = "Texture Color",
						hasAlpha = false,
						get = function(info)
							local t = OzCooldownsOptions[info[#info]]
							return t.r, t.g, t.b, t.a
						end,
						set = function(info, r, g, b)
							OzCooldownsOptions[info[#info]] = {}
							local t = OzCooldownsOptions[info[#info]]
							t.r, t.g, t.b = r, g, b
						end,
						disabled = function() return not OzCooldownsOptions["StatusBar"] or OzCooldownsOptions["FadeMode"] == "GreenToRed" or OzCooldownsOptions["FadeMode"] == "RedToGreen" end,
					},
				},
			},
			reset = {
				type = 'execute',
				order = 5,
				name = 'Reset Settings',
				desc = CONFIRM_RESET_SETTINGS,
				confirm = true,
				func = function()
					OzCooldownsOptions = CopyTable(Defaults)
					OzCooldownsSpellCDs = CopyTable(SpellDefaults)
					OzCooldownsOptions["Version"] = 3
				end,
			}
		},
	}
	if EP then
		local Ace3OptionsPanel = IsAddOnLoaded("ElvUI") and ElvUI[1] or Enhanced_Config[1]
		Ace3OptionsPanel.Options.args.ozcooldowns = Options
	else
		local ACR, ACD = LibStub("AceConfigRegistry-3.0", true), LibStub("AceConfigDialog-3.0", true)
		if not (ACR or ACD) then return end
		ACR:RegisterOptionsTable("BrokerLDB", Options)
		ACD:AddToBlizOptions("OzCooldowns", "OzCooldowns", nil, "spells")
		for k, v in pairs(Options.args) do
			if k ~= 'spells' then
				ACD:AddToBlizOptions("OzCooldowns", v.name, "OzCooldowns", k)
			end
		end
	end
end

local ResetVars = false
OzCooldowns:SetFrameStrata("BACKGROUND")
OzCooldowns:SetSize(40, 40)
OzCooldowns:RegisterEvent("ADDON_LOADED")
OzCooldowns:RegisterEvent("PLAYER_LOGIN")
OzCooldowns:RegisterEvent("PLAYER_ENTERING_WORLD")
OzCooldowns:SetScript("OnEvent", function(self, event, ...)
	if event == 'ADDON_LOADED' then
		local addon = ...
		if addon == AddOnName then
			if (not OzCooldownsOptions["Version"] or OzCooldownsOptions["Version"] < 3) then
				OzCooldownsOptions = CopyTable(Defaults)
				OzCooldownsSpellCDs = CopyTable(SpellDefaults)
				OzCooldownsOptions["Version"] = 3
				ResetVars = true
			end
			for k, v in pairs(PlayerCooldowns) do
				if not OzCooldownsSpellCDs[GetSpellInfo(v)] then
					OzCooldownsSpellCDs[GetSpellInfo(v)] = true
				end
			end
			LSM, ES, EP, AS = LibStub("LibSharedMedia-3.0"), IsAddOnLoaded('ElvUI') and ElvUI[1]:GetModule("EnhancedShadows", true), LibStub("LibElvUIPlugin-1.0", true), AddOnSkins and unpack(AddOnSkins)
			if EP then
				EP:RegisterPlugin(AddOnName, self.GetOptions)
			else
				self:GetOptions()
			end
			self:SetPoint("BOTTOM", UIParent, "BOTTOM", 0, 360)
			self:UnregisterEvent(event)
		end
	elseif event == "PLAYER_LOGIN" then
		if IsAddOnLoaded("Tukui") then
			Tukui[1]["Movers"]:RegisterFrame(self)
		elseif IsAddOnLoaded("ElvUI") then
			ElvUI[1]:CreateMover(self, "OzCooldownsMover", "OzCooldowns Anchor", nil, nil, nil, "ALL,GENERAL")
		else
			OzCooldowns:SetMovable(true)
			OzCooldowns:SetScript('OnDragStart', function(self) self:StartMoving() end)
			OzCooldowns:SetScript('OnDragStop', function(self) self:StopMovingOrSizing() end)
		end
	elseif event == 'SPELLS_CHANGED' then
		self:BuildCooldowns()
	elseif event == "PLAYER_ENTERING_WORLD" then
		self:BuildCooldowns()
		self:RegisterEvent("SPELLS_CHANGED")
		self:RegisterUnitEvent("UNIT_SPELLCAST_FAILED", 'player', 'pet')
		self:RegisterUnitEvent("UNIT_SPELLCAST_SUCCEEDED", 'player', 'pet')
		self:RegisterUnitEvent("UNIT_SPELLCAST_INTERRUPTED", 'player', 'pet')
		for _, Frame in pairs(Cooldowns) do
			if GetSpellCooldown(Frame.SpellID) then
				self:DelayedEnableCooldown(Frame)
			end
		end
		DEFAULT_CHAT_FRAME:AddMessage(format("%s by |cFFFF7D0AAzilroka|r - Version: |cff1784d1%s|r Loaded!", self["Title"], self["Version"]))
		if ResetVars then DEFAULT_CHAT_FRAME:AddMessage(format("%s Options has been reset!", self["Title"])) end
		self:UnregisterEvent(event)
	elseif strfind(event, 'UNIT_SPELLCAST_') then
		local spellID = select(5, ...)
		self:FindCooldown(spellID)
	end
end)
